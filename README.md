#Supermarket checkout system that calculates total price of products after applying discount rules if applicable.

A simple checkout system that calculates the cost of a basket and applies any special discounts or pricing rules which are required for that order.

## Installation Instructions

You will require ruby 2.4.0 installed to use this program.

Clone the application from bitbucket, cd into the directory and install all dependencies. 

```
$ git clone git@bitbucket.org:rakesh050791/wunder-assignment.git
$ cd wunder-assignment
$ bundle install
```

## Usage Instructions

The application can be run from irb. To open irb enter `$ irb` while in the project root directory.

To Run : call main.rb file : 
```
$ ruby main.rb
```

## Running Tests

To run the test suite, run rspec from the root directory.
```
$ rspec
```

Below things we are executing inside main.rb file.

```ruby
#This will import all required classes to run the program
require_relative 'lib/checkout'
require_relative 'lib/item'
require_relative 'lib/rules/multibuy'
require_relative 'lib/rules/percent_off'

#Have created some input files i.e.  items json, rules json and cart_item json to give input to the program.
 
 #Reading above mentioned files for processing
items_json = File.read("items.json")
rules_json = File.read("rules.json")
cart_items_json = File.read("cart_items.json")


#Next step is we are iterating items_json from above step & building catalog, creating Item objects & putting into products array.
JSON.parse(items_json).each do |item|
  products << Item.new(code: item["code"], name: item["name"], price: item["price"])
end


#Next step is we are iterating rules_json & building rules for discount, & putting into pricing_rules array.
# For example, if we want to apply a 10% discount to orders over €30
# Anothoer example, if We want to apply a €3 discount per item when you order 2 or more "002" items
JSON.parse(rules_json).each do |k, value|
  klass = k.split('_').collect(&:capitalize).join
  case klass
    when 'PercentOff'
      pricing_rules << eval(klass).new(percent_discount: value["percent_discount"], threshold: value["threshold"])
    when 'Multibuy'
      pricing_rules << eval(klass).new(item_code: value["item_code"], min_items: value["min_items"], discount: value["discount"])
  end
  pricing_rules
end

#Next step is we are iterating cart_items_json for the items we want to scan for an order, and creating checkout object and
#passing pricing_rules & products array to the same to display item's total after discount. 
#checkout has two below methods.
 ## #scan => takes the code of the item as a string and adds it to your basket
 ## #total => returns the cost of the current basket as a string in euro's 

JSON.parse(cart_items_json).each do |items|
  checkout = Checkout.new(pricing_rules, products: products)
  items.each {|item| checkout.scan item}

  puts "Items in cart #{items} ----> total is #{checkout.total}"
end
```

## Improvements scope

* Currently products are being represented as an array of Items and then injected into the system. In a real system these would be held as a table in a DB and accessed from DB.
* Order is being represented by a Hash within Checkout. This could be extracted to an No SQL DB like redis.


