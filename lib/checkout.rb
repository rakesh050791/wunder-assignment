require_relative 'basket'
require 'byebug'

class Checkout
  def initialize(promotional_rules = nil, products: nil)
    @products = products
    @basket = Basket.new(promotional_rules, products: products)
    @order = Hash.new(0)
  end

  def scan(item_code)
    fail "#{item_code} item scanned is not valid." unless item_present_in_products?(item_code)
    @order[item_code] += 1
  end

  def total
    '€%.2f' % order_price_in_euros
  end

  private

  attr_reader :products, :basket, :order

  def order_price_in_euros
    basket.total(order)
  end

  def item_present_in_products?(code)
    products.map{ |product| product.code }.include?(code)
  end
end