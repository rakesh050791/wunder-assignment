require_relative '../lib/basket'

describe Basket do
  let(:item_001) { double :Item, code: "001", price: 5 }
  let(:item_002) { double :Item, code: "002", price: 3 }
  let(:products) { [item_001, item_002] }
  let(:percent_discount) { double :PercentOff, apply: 0 }
  let(:promotional_rules) { [percent_discount] }

  subject(:rule_engine) { described_class.new(promotional_rules, products: products) }

  describe '#total' do
    it 'is expected to calculate basket without any bonuses' do
      order = { "001" => 1, "002" => 1 }
      expect(rule_engine.total(order)).to eq 8
    end

    it 'is expected to be able to apply a given discount' do
      allow(percent_discount).to receive(:apply).and_return 10
      order = { "002" => 4 }
      expect(rule_engine.total(order)).to eq 2
    end
  end
end
