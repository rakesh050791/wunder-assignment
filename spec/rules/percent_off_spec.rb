require_relative '../../lib/rules/percent_off'

describe PercentOff do
  subject(:percent_discount) { described_class.new(percent_discount: 10, threshold: 3) }

  describe '#apply' do
    it 'is expected to apply the percent discount on values over the minimum' do
      expect(percent_discount.apply(100, {})).to eq 10
    end

    it 'is expected to apply no discount on values below the minimum' do
      expect(percent_discount.apply(1, {})).to eq 0
    end
  end
end