require 'json'
require_relative 'lib/checkout'
require_relative 'lib/item'
require_relative 'lib/rules/multibuy'
require_relative 'lib/rules/percent_off'

items_json = File.read("items.json")
rules_json = File.read("rules.json")
cart_items_json = File.read("cart_items.json")

products, pricing_rules = [], []

JSON.parse(items_json).each do |item|
  products << Item.new(code: item["code"], name: item["name"], price: item["price"])
end


JSON.parse(rules_json).each do |k, value|
  klass = k.split('_').collect(&:capitalize).join
  case klass
    when 'PercentOff'
      pricing_rules << eval(klass).new(percent_discount: value["percent_discount"], threshold: value["threshold"])
    when 'Multibuy'
      pricing_rules << eval(klass).new(item_code: value["item_code"], min_items: value["min_items"], discount: value["discount"])
  end
  pricing_rules
end


JSON.parse(cart_items_json).each do |items|
  checkout = Checkout.new(pricing_rules, products: products)
  items.each {|item| checkout.scan item}

  puts "Items in cart #{items} ----> total is #{checkout.total}"
end
